/***************************************************************
 * Name:      HW7App.cpp
 * Purpose:   Code for Application Class
 * Author:    ll ()
 * Created:   2019-04-03
 * Copyright: ll ()
 * License:
 **************************************************************/

#include "HW7App.h"

//(*AppHeaders
#include "HW7Main.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(HW7App);

bool HW7App::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	HW7Frame* Frame = new HW7Frame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
        