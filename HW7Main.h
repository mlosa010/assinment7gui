/***************************************************************
 * Name:      HW7Main.h
 * Purpose:   Defines Application Frame
 * Author:    ll ()
 * Created:   2019-04-03
 * Copyright: ll ()
 * License:
 **************************************************************/

#ifndef HW7MAIN_H
#define HW7MAIN_H

//(*Headers(HW7Frame)
#include <wx/gauge.h>
#include <wx/listctrl.h>
#include <wx/button.h>
#include <wx/menu.h>
#include <wx/panel.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/textctrl.h>
//*)

class HW7Frame: public wxFrame
{
    public:

        HW7Frame(wxWindow* parent,wxWindowID id = -1);
        virtual ~HW7Frame();

    private:

        //(*Handlers(HW7Frame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnTextCtrl1Text(wxCommandEvent& event);
        void start(wxCommandEvent& event);
        void stop(wxCommandEvent& event);
        void selectURL(wxCommandEvent& event);
        void selectWord(wxCommandEvent& event);
        //*)

        //(*Identifiers(HW7Frame)
        static const long ID_BUTTON1;
        static const long ID_BUTTON2;
        static const long ID_TEXTCTRL1;
        static const long ID_TEXTCTRL2;
        static const long ID_BUTTON3;
        static const long ID_BUTTON4;
        static const long ID_GAUGE1;
        static const long ID_LISTCTRL1;
        static const long ID_PANEL1;
        static const long ID_MENUITEM1;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(HW7Frame)
        wxPanel* Panel1;
        wxStatusBar* StatusBar1;
        wxButton* Button4;
        wxButton* Button1;
        wxButton* Button2;
        wxButton* Button3;
        wxListCtrl* ListCtrl1;
        wxTextCtrl* TextCtrl1;
        wxGauge* Gauge1;
        wxTextCtrl* TextCtrl2;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // HW7MAIN_H
                                                                                                                