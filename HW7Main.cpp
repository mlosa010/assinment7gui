/***************************************************************
 * Name:      HW7Main.cpp
 * Purpose:   Code for Application Frame
 * Author:    ll ()
 * Created:   2019-04-03
 * Copyright: ll ()
 * License:
 **************************************************************/

#include "HW7Main.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(HW7Frame)
#include <wx/string.h>
#include <wx/intl.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(HW7Frame)
const long HW7Frame::ID_BUTTON1 = wxNewId();
const long HW7Frame::ID_BUTTON2 = wxNewId();
const long HW7Frame::ID_TEXTCTRL1 = wxNewId();
const long HW7Frame::ID_TEXTCTRL2 = wxNewId();
const long HW7Frame::ID_BUTTON3 = wxNewId();
const long HW7Frame::ID_BUTTON4 = wxNewId();
const long HW7Frame::ID_GAUGE1 = wxNewId();
const long HW7Frame::ID_LISTCTRL1 = wxNewId();
const long HW7Frame::ID_PANEL1 = wxNewId();
const long HW7Frame::ID_MENUITEM1 = wxNewId();
const long HW7Frame::idMenuAbout = wxNewId();
const long HW7Frame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(HW7Frame,wxFrame)
    //(*EventTable(HW7Frame)
    //*)
END_EVENT_TABLE()

HW7Frame::HW7Frame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(HW7Frame)
    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    Panel1 = new wxPanel(this, ID_PANEL1, wxPoint(200,264), wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    Button1 = new wxButton(Panel1, ID_BUTTON1, _("SURL"), wxPoint(40,352), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    Button2 = new wxButton(Panel1, ID_BUTTON2, _("Word"), wxPoint(40,400), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    TextCtrl1 = new wxTextCtrl(Panel1, ID_TEXTCTRL1, _("URL"), wxPoint(144,352), wxSize(192,27), 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    TextCtrl2 = new wxTextCtrl(Panel1, ID_TEXTCTRL2, _("Text"), wxPoint(144,400), wxSize(192,27), 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    Button3 = new wxButton(Panel1, ID_BUTTON3, _("Start"), wxPoint(144,312), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
    Button4 = new wxButton(Panel1, ID_BUTTON4, _("Stop"), wxPoint(248,312), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON4"));
    Gauge1 = new wxGauge(Panel1, ID_GAUGE1, 100, wxPoint(40,312), wxSize(80,28), 0, wxDefaultValidator, _T("ID_GAUGE1"));
    ListCtrl1 = new wxListCtrl(Panel1, ID_LISTCTRL1, wxPoint(40,112), wxSize(296,120), 0, wxDefaultValidator, _T("ID_LISTCTRL1"));
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, ID_MENUITEM1, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&HW7Frame::selectURL);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&HW7Frame::selectWord);
    Connect(ID_BUTTON3,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&HW7Frame::start);
    Connect(ID_BUTTON4,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&HW7Frame::stop);
    Connect(ID_MENUITEM1,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&HW7Frame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&HW7Frame::OnAbout);
    //*)
}

HW7Frame::~HW7Frame()
{
    //(*Destroy(HW7Frame)
    //*)
}

void HW7Frame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void HW7Frame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void HW7Frame::selectURL(wxCommandEvent& event)
{
}

void HW7Frame::selectWord(wxCommandEvent& event)
{
}
