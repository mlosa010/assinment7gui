/***************************************************************
 * Name:      HW7App.h
 * Purpose:   Defines Application Class
 * Author:    ll ()
 * Created:   2019-04-03
 * Copyright: ll ()
 * License:
 **************************************************************/

#ifndef HW7APP_H
#define HW7APP_H

#include <wx/app.h>

class HW7App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // HW7APP_H
